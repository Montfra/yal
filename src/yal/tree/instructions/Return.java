package yal.tree.instructions;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.expressions.Expression;
import yal.util.MIPSBuilder;

public final class Return extends Instruction {
    private final Expression expression;

    public Return(final int lineNumber, final Expression expression) {
        super(lineNumber);

        if (expression == null) {
            throw new IllegalArgumentException("null expression");
        }

        this.expression = expression;
    }

    @Override
    public void check() {
        this.expression.check();

        if (this.expression.getType() != Expression.Type.INTEGER) {
            throw new AnalyseSemantiqueException(this.lineNumber, "retourne doit retourner un entier");
        }
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        mips.print(expression.toMIPS());

        mips.buildl("move $v0, $t0");
        mips.buildl("jr $ra");

        return mips.toString();
    }
}
