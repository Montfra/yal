package yal.tree.instructions;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.expressions.Expression;
import yal.tree.tos.TOS;
import yal.tree.tos.entry.Variable;
import yal.tree.tos.symbol.Symbol;
import yal.util.MIPSBuilder;

import java.util.Arrays;

/**
 * Represents a YAL assignment instruction in the abstract tree.
 */
public final class Assignment extends Instruction {
    private final static String LABEL_PREFIX = "cd_assignment";
    private final static String LABEL_LOOP_START = "loop_start";

    /**
     * Internal counter for the labels.
     */
    private static int counter;

    /**
     * Suffix chosen for the current instance.
     */
    private final int label_suffix;

    /**
     * Offset of the variable from the address stored in the saved register $s7.
     *
     * Calculated by check method.
     */
    private int offset;

    private int noblock;

    /**
     * Name of the receiver.
     */
    private final String name;

    /**
     * Source expression.
     */
    private final Expression expression;

    private final int[] stack;

    /**
     * Assignment constructor.
     *  @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param name Name of the receiver. Must be non null.
     * @param expression Source expression. Must be non null. Must be of type INTEGER.
     */
    public Assignment(final int lineNumber, final String name, final Expression expression) {
        super(lineNumber);

        if (name == null) { // name must not be null
            throw new IllegalArgumentException("null name");
        }

        if (expression == null) { // expression must not be null
            throw new IllegalArgumentException("null expression");
        }

        if (expression.getType() != Expression.Type.INTEGER) { // expression must be of type INTEGER
            throw new AnalyseSemantiqueException(lineNumber, String.format("type invalide: %s", expression.getType().name()));
        }

        this.name = name;
        this.expression = expression;
        this.stack = TOS.getInstance().getBlock();
        this.label_suffix = Assignment.counter++;
    }

    @Override
    public void check() {
        this.expression.check();

        final yal.tree.tos.symbol.Variable symbol = TOS.getInstance().lookup(this.lineNumber, new Variable(this.name), stack);

        if (symbol.getClass() == yal.tree.tos.symbol.Variable.class) {
            this.offset = symbol.getOffset();
            this.noblock = symbol.getNoblock();
        } else {
            throw new AnalyseSemantiqueException(lineNumber, "assignment can only be to a variable");
        }
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        if (this.noblock == this.stack[this.stack.length - 1]) {
            // puts the result of the expression in $t0
            mips.print(this.expression.toMIPS()); // delegates to expression
            // $t0: result of the left expression

            mips.buildf(String.format("sw $t0, %d($s7)", -this.offset)); // moves the result of the expresion to the receiver's location in memory
        } else {
            mips.buildf("li $t1, %d", this.noblock);
            mips.buildl("move $t0, $s7");
            mips.label(Assignment.LABEL_PREFIX, Assignment.LABEL_LOOP_START, this.label_suffix);
            mips.buildl("lw $s6, 8($t0)");
            mips.buildl("lw $t0, 4($t0)");
            mips.buildf("bne $t0, $t1, %s_%s_%d", Assignment.LABEL_PREFIX, Assignment.LABEL_LOOP_START, this.label_suffix);
            mips.buildf("lw $t0, %d($s6)", -this.offset);

            // puts the result of the expression in $t0
            mips.print(this.expression.toMIPS()); // delegates to expression
            // $t0: result of the left expression

            mips.buildf(String.format("sw $t0, %d($s6)", -this.offset)); // moves the result of the expresion to the receiver's location in memory
        }

        return mips.toString();
    }
}
