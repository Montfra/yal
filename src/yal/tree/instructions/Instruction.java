package yal.tree.instructions;

import yal.tree.AbstractTree;

/**
 * Represents a YAL instruction in the abstract tree.
 */
public abstract class Instruction extends AbstractTree {
    /**
     * Instruction constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     */
    protected Instruction(final int lineNumber) {
        super(lineNumber);
    }
}
