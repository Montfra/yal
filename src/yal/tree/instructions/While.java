package yal.tree.instructions;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.InstructionsBlock;
import yal.tree.expressions.Expression;
import yal.util.MIPSBuilder;

/**
 * A YAL while loop instruction.
 */
public final class While extends Instruction {
    private final static String LABEL_PREFIX = "while";
    private final static String LABEL_NAME_ENTRY = "entry";
    private final static String LABEL_NAME_END = "end";

    /**
     * Internal counter for the labels.
     */
    private static int counter;

    /**
     * Suffix chosen for the current instance.
     */
    private final int label_suffix;

    /**
     * Conditional expression.
     */
    private final Expression conditional;

    private final InstructionsBlock instructions;

    /**
     * While constructor.
     *
     * @param lineNumber Line number of the current node of the abstract tree. Must not be negative.
     * @param conditional Conditional expression. Must not be null. Must be of type BOOLEAN.
     * @param instructions Instructions to execute while the conditional evaluates to true.
     */
    public While(final int lineNumber, final Expression conditional, final InstructionsBlock instructions) {
        super(lineNumber);

        if (conditional == null) { // conditional must not be null
            throw new IllegalArgumentException("null conditional");
        }

        if (conditional.getType() != Expression.Type.BOOLEAN) { // conditional must be of type BOOLEAN
            throw new AnalyseSemantiqueException(lineNumber, String.format("type invalide: %s", conditional.getType().name()));
        }

        if (instructions == null) { // instructions must not be null
            throw new IllegalArgumentException("null instructions");
        }

        this.conditional = conditional;
        this.instructions = instructions;

        this.label_suffix = While.counter++;
    }

    @Override
    public void check() {
        this.conditional.check();
        this.instructions.check();
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        mips.label(While.LABEL_PREFIX, While.LABEL_NAME_ENTRY, this.label_suffix); // entry point of the loop
        // puts the result of the conditional in $t0
        mips.print(this.conditional.toMIPS()); // delegates to expression
        // $t0: result of the left expression
        mips.buildf("beq $zero, $t0, %s_%s_%d", While.LABEL_PREFIX, While.LABEL_NAME_END, this.label_suffix); // jumps to the end of the loop if the conditional evaluates to false
        mips.print(this.instructions.toMIPS()); // delegate to instructionsConditionTrue
        mips.buildf("j %s_%s_%d", While.LABEL_PREFIX, While.LABEL_NAME_ENTRY, this.label_suffix); // jump to the entry point of the loop
        mips.label(While.LABEL_PREFIX, While.LABEL_NAME_END, this.label_suffix); // end of the loop

        return mips.toString();
    }
}
