package yal.tree.instructions.io;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.instructions.Instruction;
import yal.tree.tos.TOS;
import yal.tree.tos.symbol.Variable;
import yal.util.MIPSBuilder;

import java.util.Arrays;

/**
 * Represents a YAL read instruction in the abstract tree.
 */
public final class Read extends Instruction {
    /**
     * Offset of the variable from the address stored in the saved register $s7.
     *
     * Calculated by check method.
     */
    private int offset;

    /**
     * Name of the variable.
     */
    private final String name;
    private final int[] stack;

    /**
     * Read constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param name Name of the variable that is to be read into. Must not be null.
     */
    public Read(final int lineNumber, final String name) {
        super(lineNumber);

        if (name == null) { // name must not be null
            throw new IllegalArgumentException("null name");
        }

        this.name = name;
        this.stack = TOS.getInstance().getBlock();
    }

    @Override
    public void check() {
        final Variable symbol = TOS.getInstance().lookup(this.lineNumber, new yal.tree.tos.entry.Variable(this.name), stack);

        if (symbol.getClass() == yal.tree.tos.symbol.Variable.class) {
            this.offset = symbol.getOffset();
        } else {
            throw new AnalyseSemantiqueException(lineNumber, "read can only be applied to variables");
        }
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        mips.buildl("li $v0, 0x5"); // loads the syscall code for reading integers
        mips.buildl("syscall"); // read an integer
        mips.buildf("sw $v0, %d($s7)", -this.offset); // stores the read integer into the variable's memory.

        return mips.toString();
    }
}
