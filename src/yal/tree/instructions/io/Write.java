package yal.tree.instructions.io;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.expressions.Expression;
import yal.tree.instructions.Instruction;
import yal.util.MIPSBuilder;

/**
 * Represents a YAL write instruction in the abstract tree.
 */
public final class Write extends Instruction {
    private final static String LABEL_PREFIX = "write";
    private final static String LABEL_NAME_FALSE = "false";
    private final static String LABEL_NAME_END = "end";

    /**
     * Internal counter for the labels.
     */
    private static int counter;

    /**
     * Suffix chosen for the current instance.
     */
    private final int label_suffix;

    /**
     * Expression to write.
     */
    private final Expression expression;

    /**
     * Write constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param expression Expression to write. Must be non null. Must be of type BOOLEAN or INTEGER.
     */
    public Write(final int lineNumber, final Expression expression) {
        super(lineNumber);

        if (expression == null) { // expression must not be null
            throw new IllegalArgumentException("null expression");
        }

        if (expression.getType() != Expression.Type.INTEGER && expression.getType() != Expression.Type.BOOLEAN) { // expression must be of type BOOLEAN or INTEGER
            throw new AnalyseSemantiqueException(lineNumber, "%s: type non supporte pour l'ecriture");
        }

        this.expression = expression;

        this.label_suffix = counter++;
    }

    @Override
    public void check() {
        this.expression.check();
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        // puts the result of the conditional in $t0
        mips.print(this.expression.toMIPS()); // delegates to expression
        // $t0: result of the left expression

        if (this.expression.getType() == Expression.Type.BOOLEAN) { // "true" or "false" for booleans
            mips.buildf("beq $zero, $t0, %s_%s_%d", Write.LABEL_PREFIX, Write.LABEL_NAME_FALSE, this.label_suffix); // jump to the false body if the boolean evaluates to false
            mips.buildl("la $a0, boolean_true"); // loads the address of the string for true
            mips.buildf("j %s_%s_%d", Write.LABEL_PREFIX, Write.LABEL_NAME_END, this.label_suffix); // jump to the end of the if
            mips.label(Write.LABEL_PREFIX, Write.LABEL_NAME_FALSE, this.label_suffix); // false body
            mips.buildl("la $a0, boolean_false"); // loads the address of the string for false
            mips.label(Write.LABEL_PREFIX, Write.LABEL_NAME_END, this.label_suffix); // end of the if
            mips.buildl("li $v0, 0x4"); // load the syscall for printing string
            mips.buildl("syscall"); // prints the boolean
        } else if (this.expression.getType() == Expression.Type.INTEGER) { // value for integers
            mips.buildl("move $a0, $t0"); // moves the result of the expression to $a0
            mips.buildl("li $v0, 0x1"); // loads the syscall code for printing integer
            mips.buildl("syscall"); // prints the integer
        }

        mips.buildl("li $a0, 0xA"); // loads the newline character
        mips.buildl("li $v0, 0xB"); // loads the syscall code for printing a character
        mips.buildl("syscall"); // prints the newline character

        return mips.toString();
    }
}
