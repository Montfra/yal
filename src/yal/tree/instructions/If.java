package yal.tree.instructions;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.InstructionsBlock;
import yal.tree.expressions.Expression;
import yal.util.MIPSBuilder;

/**
 * A YAL if conditional instruction.
 */
public final class If extends Instruction {
    private final static String LABEL_PREFIX = "if";
    private final static String LABEL_NAME_ELSE = "else";
    private final static String LABEL_NAME_END = "end";

    /**
     * Internal counter for the labels.
     */
    private static int counter;

    /**
     * Suffix chosen for the current instance.
     */
    private final int label_suffix;

    /**
     * Conditional expression.
     */
    private final Expression conditional;

    private final InstructionsBlock instructionsConditionTrue;
    private final InstructionsBlock instructionsConditionFalse;

    /**
     * If constructor.
     *
     * @param lineNumber Line number of the current node of the abstract tree. Must not be negative.
     * @param conditional Conditional expression. Must not be null. Must be of type BOOLEAN.
     * @param instructionsTrue Instructions to execute if the conditional evaluates to true.
     * @param instructionsFalse Instructions to execute if the conditional evaluates to false.
     */
    public If(final int lineNumber, final Expression conditional, final InstructionsBlock instructionsTrue, final InstructionsBlock instructionsFalse) {
        super(lineNumber);

        if (conditional == null) { // conditional must not be null
            throw new IllegalArgumentException("null expression");
        }

        if (conditional.getType() != Expression.Type.BOOLEAN) { // conditional must be of type BOOLEAN
            throw new AnalyseSemantiqueException(lineNumber, String.format("type invalide: %s", conditional.getType().name()));
        }

        if (instructionsTrue == null) { // instructionsTrue must not be null
            throw new IllegalArgumentException("null expression");
        }

        this.conditional = conditional;
        this.instructionsConditionTrue = instructionsTrue;
        this.instructionsConditionFalse = instructionsFalse;

        this.label_suffix = If.counter++;
    }

    /**
     * If constructor.
     *
     * @param lineNumber Line number of the current node of the abstract tree. Must not be negative.
     * @param conditional Conditional expression. Must not be null. Must be of type BOOLEAN.
     * @param instructions Instructions to execute if the conditional evaluates to true.
     */
    public If(final int lineNumber, final Expression conditional, final InstructionsBlock instructions) {
        this(lineNumber, conditional, instructions, null);
    }

    @Override
    public void check() {
        this.conditional.check();
        this.instructionsConditionTrue.check();

        if (this.instructionsConditionFalse != null) {
            this.instructionsConditionFalse.check();
        }
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        // puts the result of the conditional in $t0
        mips.print(this.conditional.toMIPS()); // delegates to expression
        // $t0: result of the left expression

        if (this.instructionsConditionFalse == null) { // without else construction
            mips.buildf("beq $zero, $t0, %s_%s_%d", If.LABEL_PREFIX, If.LABEL_NAME_END, this.label_suffix); // jump to the end of the if if the conditional evaluates to false
            mips.print(this.instructionsConditionTrue.toMIPS()); // delegate to instructionsConditionTrue
            mips.buildf("%s_%s_%d:", If.LABEL_PREFIX, If.LABEL_NAME_END, this.label_suffix); // end of the if
        } else { // with else construction
            mips.buildf("beq $zero, $t0, %s_%s_%d", If.LABEL_PREFIX, If.LABEL_NAME_ELSE, this.label_suffix); // jump to the else body if the conditional evaluates to false
            mips.print(this.instructionsConditionTrue.toMIPS()); // delegate to instructionsConditionTrue
            mips.buildf("j %s_%s_%d", If.LABEL_PREFIX, If.LABEL_NAME_END, this.label_suffix); // jump to the end of the if
            mips.buildf("%s_%s_%d:", If.LABEL_PREFIX, If.LABEL_NAME_ELSE, this.label_suffix); // else body
            mips.print(this.instructionsConditionFalse.toMIPS()); // delegate to instructionsConditionFalse
            mips.buildf("%s_%s_%d:", If.LABEL_PREFIX, If.LABEL_NAME_END, this.label_suffix); // end of the if
        }

        return mips.toString();
    }
}
