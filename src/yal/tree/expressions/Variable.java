package yal.tree.expressions;

import yal.util.MIPSBuilder;
import yal.tree.tos.TOS;

import java.util.Arrays;

/**
 * Represents a YAL variable as an expression in the abstract tree.
 *
 * An expression stores its result in the temporary register $t0
 */
public final class Variable extends Expression {
    private final static String LABEL_PREFIX = "cd_variable";
    private final static String LABEL_LOOP_START = "loop_start";

    /**
     * Internal counter for the labels.
     */
    private static int counter;

    /**
     * Suffix chosen for the current instance.
     */
    private final int label_suffix;

    /**
     * Name of the variable.
     */
    private final String name;

    private final int[] stack;

    private yal.tree.tos.symbol.Variable symbol;

    /**
     * Variable constructor.
     *  @param lineNumber Line number of the current node of the abstract tree.
     * @param name Name of the variable. Must be non null.
     */
    public Variable(final int lineNumber, final String name) {
        super(lineNumber, Type.INTEGER);

        if (name == null) { // name must not be null
            throw new IllegalArgumentException("null name");
        }

        this.name = name;
        this.stack = TOS.getInstance().getBlock();
        this.label_suffix = Variable.counter++;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public void check() {
        this.symbol = TOS.getInstance().lookup(this.lineNumber, new yal.tree.tos.entry.Variable(this.name), this.stack);
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        if (this.symbol.getNoblock() == this.stack[this.stack.length - 1]) {
            mips.buildf("lw $t0, %d($s7)", -this.symbol.getOffset());
        } else {
            mips.buildf("li $t1, %d", this.symbol.getNoblock());
            mips.buildl("move $t0, $s7");
            mips.label(Variable.LABEL_PREFIX, Variable.LABEL_LOOP_START, this.label_suffix);
            mips.buildl("lw $s6, 8($t0)");
            mips.buildl("lw $t0, 4($t0)");
            mips.buildf("bne $t0, $t1, %s_%s_%d", Variable.LABEL_PREFIX, Variable.LABEL_LOOP_START, this.label_suffix);
            mips.buildf("lw $t0, %d($s6)", -this.symbol.getOffset());
        }

        return mips.toString();
    }
}
