package yal.tree.expressions.literals;

import yal.util.MIPSBuilder;

/**
 * A YAL integer literal expression node in the abstract tree.
 *
 * Once compiled to MIPS, stores its value in the temporary register $t0.
 */
public final class IntegerLiteral extends Literal {
    private final static String MAX_INT = "2147483647";
    private final static String MIN_INT = "-2147483648";

    /**
     * Integer literal value.
     */
    private final int value;

    /**
     * IntegerLiteral constructor.
     *
     * Only 32 bits decimal integers are supported.
     * Must be between -2^31 and 2^31 - 1.
     *
     * @param lineNumber Line number of the current node of the abstract tree.
     * @param rawLiteral Raw literal text. Must be non null and must be a valid 32 bits decimal integer.
     */
    public IntegerLiteral(final int lineNumber, final String rawLiteral) {
        super(lineNumber, Type.INTEGER);

        if (rawLiteral.length() == 0) { // rawLiteral must be a valid integer (length check to avoid NullPointerException)
            throw new IllegalArgumentException("rawLiteral is the empty string");
        }

        this.value = Integer.parseInt(rawLiteral);
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        // put the integer value in $t0
        mips.buildf("li $t0, %d", this.value);
        // $t0: value of the literal

        return mips.toString();
    }
}
