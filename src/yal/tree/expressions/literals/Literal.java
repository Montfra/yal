package yal.tree.expressions.literals;

import yal.tree.expressions.Expression;

/**
 * Represents a YAL literal in the abstract tree.
 *
 * An expression stores its result in the temporary register $t0.
 */
public abstract class Literal extends Expression {
    /**
     * Literal constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param type Type of the expression. Must not be null.
     */
    public Literal(final int lineNumber, final Expression.Type type) {
        super(lineNumber, type);
    }

    @Override
    public final void check() {
        // do nothing
    }
}
