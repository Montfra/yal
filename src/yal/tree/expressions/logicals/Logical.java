package yal.tree.expressions.logicals;

import yal.tree.expressions.Expression;

/**
 * Represents a YAL logical expression in the abstract tree.
 * 0 represents false and 1 represents true.
 *
 * An expression stores its result in the temporary register $t0.
 */
public abstract class Logical extends Expression {
    /**
     * Logical constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     */
    protected Logical(final int lineNumber) {
        super(lineNumber, Type.BOOLEAN);
    }
}
