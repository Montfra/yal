package yal.tree.expressions.logicals.binary;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.expressions.Expression;
import yal.tree.expressions.logicals.Logical;

/**
 * Represents a YAL binary logical expression in the abstract tree.
 * 0 represents false and 1 represents true.
 *
 * An expression stores its result in the temporary register $t0.
 */
public abstract class Binary extends Logical {
    /**
     * Left expression.
     */
    protected final Expression leftExpression;

    /**
     * Right expression.
     */
    protected final Expression rightExpression;

    /**
     * Binary constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param leftExpression Left expression. Must be non null. Must be of type BOOLEAN.
     * @param rightExpression Right expression. Must be non null. Must be of type BOOLEAN.
     */
    public Binary(final int lineNumber, final Expression leftExpression, final Expression rightExpression) {
        super(lineNumber);

        if (leftExpression == null) { // leftExpression must be non null
            throw new IllegalArgumentException("null leftExpression");
        }

        if (leftExpression.getType() != Type.BOOLEAN) { // leftExpression must be of type BOOLEAN
            throw new AnalyseSemantiqueException(lineNumber, String.format("type invalide: %s", leftExpression.getType().name()));
        }

        if (rightExpression == null) { // rightExpression must be non null
            throw new IllegalArgumentException("null rightExpression");
        }

        if (rightExpression.getType() != Type.BOOLEAN) { // rightExpression must be of type BOOLEAN
            throw new AnalyseSemantiqueException(lineNumber, String.format("type invalide: %s", rightExpression.getType().name()));
        }

        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public final void check() {
        this.leftExpression.check();

        this.rightExpression.check();
    }
}
