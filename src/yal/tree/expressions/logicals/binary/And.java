package yal.tree.expressions.logicals.binary;

import yal.tree.expressions.Expression;
import yal.util.MIPSBuilder;

/**
 * Represents a YAL logical and expression in the abstract tree.
 * 0 represents false and 1 represents true.
 *
 * An expression stores its result in the temporary register $t0.
 */
public final class And extends Binary {
    private final static String LABEL_PREFIX = "and";
    private final static String LABEL_NAME_SKIP = "skip";

    /**
     * Internal counter for the labels.
     */
    private static int counter;

    /**
     * Suffix chosen for the current instance.
     */
    private final int label_suffix;

    /**
     * And constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param leftExpression Left expression. Must be non null. Must be of type BOOLEAN.
     * @param rightExpression Right expression. Must be non null. Must be of type BOOLEAN.
     */
    public And(final int lineNumber, final Expression leftExpression, final Expression rightExpression) {
        super(lineNumber, leftExpression, rightExpression);

        this.label_suffix = And.counter++;
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        // puts the result of the left expression in $t0
        mips.print(this.leftExpression.toMIPS()); // delegates to leftExpression
        // $t0: result of the left expression

        // if the left expression evaluates to false, there is no need to evaluate the right expression
        mips.buildf("beq $zero, $t0, %s_%s_%d", And.LABEL_PREFIX, And.LABEL_NAME_SKIP, this.label_suffix); // branches to the end of the expression if the left expression evaluates to false (which is all that is needed because the result false is still in $t0)

        // puts the result of the right expression in $t0
        mips.print(this.rightExpression.toMIPS()); // delegates to rightExpression
        // $t0: result of the right expression

        // there is no need to check the value of the right expression because if it reached this point, the
        //     expression's result is the right expressions's result, which is already in $t0

        // label used to skip the evaluation of the right expression if needed
        mips.label(And.LABEL_PREFIX, And.LABEL_NAME_SKIP, this.label_suffix);

        return mips.toString();
    }
}
