package yal.tree.expressions.logicals.unary;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.expressions.Expression;
import yal.tree.expressions.logicals.Logical;

/**
 * Represents a YAL unary logical expression in the abstract tree.
 * 0 represents false and 1 represents true.
 *
 * An expression stores its result in the temporary register $t0.
 */
public abstract class Unary extends Logical {
    /**
     * Expression that the operator must be applied to.
     */
    protected final Expression expression;

    /**
     * Unary constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param expression Expression. Must be non null. Must be of type BOOLEAN.
     */
    Unary(final int lineNumber, final Expression expression) {
        super(lineNumber);

        if (expression == null) { // expression must be non null
            throw new IllegalArgumentException("null expression");
        }

        if (expression.getType() != Type.BOOLEAN) { // expression must be of type BOOLEAN
            throw new AnalyseSemantiqueException(lineNumber, String.format("type invalide: %s", expression.getType().name()));
        }

        this.expression = expression;
    }

    @Override
    public final void check() {
        this.expression.check();
    }
}
