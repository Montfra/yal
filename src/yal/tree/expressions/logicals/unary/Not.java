package yal.tree.expressions.logicals.unary;

import yal.util.MIPSBuilder;
import yal.tree.expressions.Expression;

/**
 * Represents a YAL logical not expression in the abstract tree.
 * 0 represents false and 1 represents true.
 *
 * An expression stores its result in the temporary register $t0.
 */
public final class Not extends Unary {
    /**
     * Not constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param expression Expression. Must be non null. Must be of type BOOLEAN.
     */
    public Not(final int lineNumber, final Expression expression) {
        super(lineNumber, expression);
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        // puts the result of the expression in $t0
        mips.print(this.expression.toMIPS()); // delegates to expression
        // $t0: result of the expression

        // evaluates the expression
        mips.buildl("xori, $t0, $t0, 0x1"); // not is equivalent to xor 1
        // $t0: not expression

        return mips.toString();
    }
}
