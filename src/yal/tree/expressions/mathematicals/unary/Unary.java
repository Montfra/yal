package yal.tree.expressions.mathematicals.unary;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.expressions.Expression;
import yal.tree.expressions.mathematicals.Mathematical;

/**
 * Represents a YAL unary mathematical expression in the abstract tree.
 *
 * An expression stores its result in the temporary register $t0.
 */
public abstract class Unary extends Mathematical {
    /**
     * Expression that the operator must be applied to.
     */
    protected final Expression expression;

    /**
     * Unary constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param expression Expression. Must be non null. Must be of type INTEGER.
     */
    Unary(final int lineNumber, final Expression expression) {
        super(lineNumber);

        if (expression == null) { // expression must be non null
            throw new IllegalArgumentException("null expression");
        }

        if (expression.getType() != Type.INTEGER) { // expression must be of type INTEGER
            throw new AnalyseSemantiqueException(lineNumber, String.format("type invalide: %s", expression.getType().name()));
        }

        this.expression = expression;
    }

    @Override
    public final void check() {
        this.expression.check();
    }
}
