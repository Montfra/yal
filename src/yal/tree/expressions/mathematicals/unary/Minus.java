package yal.tree.expressions.mathematicals.unary;

import yal.util.MIPSBuilder;
import yal.tree.expressions.Expression;

/**
 * Represents a YAL opposite expression in the abstract tree.
 *
 * An expression stores its result in the temporary register $t0.
 */
public final class Minus extends Unary {
    /**
     * Minus constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param expression Expression that is to be operated. Must be non null. Must be of type INTEGER.
     */
    public Minus(final int lineNumber, final Expression expression) {
        super(lineNumber, expression);
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        // puts the result of the expression in $t0
        mips.print(this.expression.toMIPS()); // delegates to expression
        // $t0: result of the left expression

        // evaluates the result of the expression
        mips.buildl("sub $t0, $zero, $t0"); // 0 - expression
        // $t0: -expression

        return mips.toString();
    }
}
