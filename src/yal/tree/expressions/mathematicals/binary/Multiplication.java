package yal.tree.expressions.mathematicals.binary;

import yal.util.MIPSBuilder;
import yal.tree.expressions.Expression;

/**
 * Represents a YAL multiplication expression in the abstract tree.
 *
 * An expression stores its result in the temporary register $t0.
 */
public final class Multiplication extends Binary {
    /**
     * Multiplication constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param leftExpression Left expression. Must be non null. Must be of type INTEGER.
     * @param rightExpression Right expression. Must be non null. Must be of type INTEGER.
     */
    public Multiplication(final int lineNumber, final Expression leftExpression, final Expression rightExpression) {
        super(lineNumber, leftExpression, rightExpression);
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        // puts the result of the left expression in $t0
        mips.print(this.leftExpression.toMIPS()); // delegates to leftExpression
        // $t0: result of the left expression

        // pushes the result of the left expression onto the stack
        mips.buildl("sw $t0, 0($sp)"); // pushes the word stored in $t0 to the top of the stack
        mips.buildl("addi $sp, $sp, -4"); // increments the stack pointer by the size of a word

        // puts the result of the right expression in $t0
        mips.print(this.rightExpression.toMIPS()); // delegates to rightExpression
        // $t0: result of the right expression

        // pulls the result of the left expression from the stack to $t1
        mips.buildl("addi $sp, $sp, 4"); // decrements the top of the stack by one word
        mips.buildl("lw $t1, 0($sp)"); // pulls a word from the top of the stack
        // $t0: result of the right expression
        // $t1: result of the left expression

        // puts the result of the addition in $t0
        mips.buildl("mult $t1, $t0"); // multiplies the result of the left expressions by result of the right expression
        mips.buildl("mflo $t0"); // moves the least significant word of the result to $t0
        // $t0: leftExpression * rightExpression

        return mips.toString();
    }
}
