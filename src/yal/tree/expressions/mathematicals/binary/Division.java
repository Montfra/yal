package yal.tree.expressions.mathematicals.binary;

import yal.tree.expressions.Expression;
import yal.util.MIPSBuilder;

/**
 * Represents a YAL division expression in the abstract tree.
 *
 * Division by 0 will result in the program termination.
 *
 * An expression stores its result in the temporary register $t0.
 */
public final class Division extends Binary {
    /**
     * Division constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param leftExpression Left expression. Must be non null. Must be of type INTEGER.
     * @param rightExpression Right expression. Must be non null. Must be of type INTEGER.
     */
    public Division(final int lineNumber, final Expression leftExpression, final Expression rightExpression) {
        super(lineNumber, leftExpression, rightExpression);
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        // for optimization's sake, the right expression's result could be calculated first to be checked
        //     against 0 before calculating the left expression's result, but it could have unwanted effects on
        //     the result if the left expressions affects variables used by the right expression.

        // puts the result of the left expression in $t0
        mips.print(this.leftExpression.toMIPS()); // delegates to leftExpression
        // $t0: result of the left expression

        // pushes the result of the left expression onto the stack
        mips.buildl("sw $t0, 0($sp)"); // pushes the word stored in $t0 to the top of the stack
        mips.buildl("addi $sp, $sp, -4"); // increments the stack pointer by the size of a word

        // puts the result of the right expression in $t0
        mips.print(this.rightExpression.toMIPS()); // delegates to rightExpression
        // $t0: result of the right expression

        // check for divide by 0
        mips.buildl("beq $zero, $t0, runtime_error"); // if the right expressions evaluates to 0, stops the execution

        // pulls the result of the left expression from the stack to $t1
        mips.buildl("addi $sp, $sp, 4"); // decrements the top of the stack by one word
        mips.buildl("lw $t1, 0($sp)"); // pulls a word from the top of the stack
        // $t0: result of the right expression
        // $t1: result of the left expression

        // puts the result of the addition in $t0
        mips.buildl("div $t1, $t0"); // divides the result of the left expressions by result of the right expression
        mips.buildl("mflo $t0"); // moves the least significant word of the result to $t0
        // $t0: leftExpression / rightExpression

        return mips.toString();
    }
}
