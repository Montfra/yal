package yal.tree.expressions.mathematicals;

import yal.tree.expressions.Expression;

/**
 * Represents a YAL mathematical expression in the abstract tree.
 *
 * An expression stores its result in the temporary register $t0.
 */
public abstract class Mathematical extends Expression {
    /**
     * Mathematical constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     */
    protected Mathematical(final int lineNumber) {
        super(lineNumber, Type.INTEGER);
    }
}
