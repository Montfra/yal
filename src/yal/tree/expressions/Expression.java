package yal.tree.expressions;

import yal.tree.AbstractTree;

/**
 * Represents a YAL expression in the abstract tree.
 *
 * An expression stores its result in the temporary register $t0.
 */
public abstract class Expression extends AbstractTree {
    /**
     * Type of an expression.
     */
    public enum Type {
        INTEGER, BOOLEAN
    }

    /**
     * Type of the expression.
     */
    private final Type type;

    /**
     * Expression constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param type Type of the expression. Must be non null.
     */
    protected Expression(final int lineNumber, final Type type) {
        super(lineNumber);

        if (type == null) { // type must be non null
            throw new IllegalArgumentException("null type");
        }

        this.type = type;
    }

    /**
     * Returns the type of the expression.
     *
     * @return Type of the expression.
     */
    public final Type getType() {
        return this.type;
    }
}
