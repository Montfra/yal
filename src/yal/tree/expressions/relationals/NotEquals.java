package yal.tree.expressions.relationals;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.expressions.Expression;
import yal.util.MIPSBuilder;

/**
 * Represents a YAL not equals expression in the abstract tree.
 *
 * An expression stores its result in the temporary register $t0.
 */
public final class NotEquals extends Relational {
    private final static String LABEL_PREFIX = "notequals";
    private final static String LABEL_NAME_IF = "if";
    private final static String LABEL_NAME_END = "end";

    /**
     * Internal counter for the labels.
     */
    private static int counter;

    /**
     * Suffix chosen for the current instance.
     */
    private final int label_suffix;

    /**
     * NotEquals constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param leftExpression Left expression. Must be non null. Must be of type INTEGER.
     * @param rightExpression Right expression. Must be non null. Must be of type INTEGER.
     */
    public NotEquals(final int lineNumber, final Expression leftExpression, final Expression rightExpression) {
        super(lineNumber, leftExpression, rightExpression);

        if (leftExpression.getType() != rightExpression.getType()) { // leftExpression and rightExpression must be of the same type
            throw new AnalyseSemantiqueException(lineNumber, String.format("type incompatibles: %s et %s", leftExpression.getType().name(), rightExpression.getType().name()));
        }

        this.label_suffix = NotEquals.counter++;
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        // puts the result of the left expression in $t0
        mips.print(this.leftExpression.toMIPS()); // delegates to leftExpression
        // $t0: result of the left expression

        // pushes the result of the left expression onto the stack
        mips.buildl("sw $t0, 0($sp)"); // pushes the word stored in $t0 to the top of the stack
        mips.buildl("addi $sp, $sp, -4"); // increments the stack pointer by the size of a word

        // puts the result of the right expression in $t0
        mips.print(this.rightExpression.toMIPS()); // delegates to rightExpression
        // $t0: result of the right expression

        // pulls the result of the left expression from the stack to $t1
        mips.buildl("addi $sp, $sp, 4"); // decrements the top of the stack by one word
        mips.buildl("lw $t1, 0($sp)"); // pulls a word from the top of the stack
        // $t0: result of the right expression
        // $t1: result of the left expression

        // puts the result of the equality check in $t0
        mips.buildf("bne $t1, $t0, %s_%s_%d", NotEquals.LABEL_PREFIX, NotEquals.LABEL_NAME_IF, this.label_suffix); // if both expressions evaluates to a different value, jumps to the if label
        mips.buildl("li $t0, 0x0"); // puts false in $t0
        mips.buildf("j %s_%s_%d", NotEquals.LABEL_PREFIX, NotEquals.LABEL_NAME_END, this.label_suffix); // jumps to the end label
        mips.label(NotEquals.LABEL_PREFIX, NotEquals.LABEL_NAME_IF, this.label_suffix); // if label
        mips.buildl("li $t0, 0x1"); // puts true in $t0
        mips.label(NotEquals.LABEL_PREFIX, NotEquals.LABEL_NAME_END, this.label_suffix); // end label
        // $t0: leftExpression != rightExpression

        return mips.toString();
    }
}
