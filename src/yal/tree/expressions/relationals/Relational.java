package yal.tree.expressions.relationals;

import yal.tree.expressions.Expression;

/**
 * Represents a YAL relational expression in the abstract tree.
 *
 * An expression stores its result in the temporary register $t0.
 */
public abstract class Relational extends Expression {
    /**
     * Left expression.
     */
    protected final Expression leftExpression;

    /**
     * Right expression.
     */
    protected final Expression rightExpression;

    /**
     * Relational constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     * @param leftExpression Left expression. Must be non null.
     * @param rightExpression Right expression. Must be non null.
     */
    public Relational(final int lineNumber, final Expression leftExpression, final Expression rightExpression) {
        super(lineNumber, Type.BOOLEAN);

        if (leftExpression == null) { // leftExpression must be non null
            throw new IllegalArgumentException("null leftExpression");
        }

        if (rightExpression == null) { // rightExpression must be non null
            throw new IllegalArgumentException("null rightExpression");
        }

        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public final void check() {
        leftExpression.check();

        rightExpression.check();
    }
}
