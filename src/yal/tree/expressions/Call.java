package yal.tree.expressions;

import yal.exceptions.FunctionArgumentException;
import yal.tree.AbstractTree;
import yal.tree.tos.TOS;
import yal.tree.tos.symbol.Function;
import yal.util.MIPSBuilder;

import java.util.ArrayList;

public class Call extends Expression {
    private final String name;
    private final int[] stack;
    private ArrayList<Expression> parameters;

    private Function symbol;

    public Call(final int lineNumber, final String name, ArrayList<Expression> p) {
        super(lineNumber, Type.INTEGER);

        this.parameters = p;
        if (p == null || p.size() == 0) {
            this.parameters = new ArrayList<>();
        }

        if (name == null) { // name must not be null
            throw new IllegalArgumentException("null name");
        }

        this.name = name;
        this.stack = TOS.getInstance().getBlock();
    }

    @Override
    public void check() {
        if ((parameters.size() != 0) && (parameters.size() != TOS.getInstance().getFunction(name).getParameters())) {
            throw new FunctionArgumentException(lineNumber);
        }

        parameters.forEach(AbstractTree::check);

        this.symbol = TOS.getInstance().lookup(this.lineNumber, new yal.tree.tos.entry.Function(this.name, TOS.getInstance().getFunction(name).getParameters()), stack);
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        // save the environment
        mips.buildl("sw $ra, 0($sp)");
        mips.buildl("sw $s7, -4($sp)");
        mips.buildf("li $t0, %d", symbol.getNoblock());
        mips.buildl("sw $t0, -8($sp)");
        mips.buildl("addi $sp, $sp, -12");

        if (TOS.getInstance().getFunction(name).getParameters() != 0) {
            for (int i = 0; i < TOS.getInstance().getFunction(name).getParameters(); i++) {
                mips.print(parameters.get(i).toMIPS());
                mips.buildf("sw $t0, %d($sp)", -i * 4);
            }
        }

        for (int i = TOS.getInstance().getFunction(name).getParameters() * 4; i < this.symbol.getSize(); i += 4) {
            mips.buildf("sw $zero, %d($sp)", i);
        }

        mips.buildf("move $s7, $sp");
        mips.buildf("addi $sp, $sp, %d", -this.symbol.getSize());

        // call function
        mips.buildf("jal %s", this.symbol.getLabel());
        mips.buildl("move $t0, $v0");

        mips.buildf("addi $sp, $sp, %d", this.symbol.getSize());

        // restore environment
        mips.buildl("addi, $sp, $sp, 12");
        mips.buildf("lw $ra, 0($sp)");
        mips.buildf("lw $s7, -4($sp)");

        return mips.toString();
    }
}
