package yal.tree;

/**
 * Represents an abstract tree node.
 */
public abstract class AbstractTree {
    /**
     * Line number that caused the creation of the node.
     * It is to be used by semantic exceptions to indicate the line of the error.
     */
    protected final int lineNumber;

    /**
     * AbstractTree constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     */
    protected AbstractTree(final int lineNumber) {
        if (lineNumber < 1) { // lineNumber must be positive
            throw new IllegalArgumentException("negative line number");
        }

        this.lineNumber = lineNumber;
    }

    /**
     * Checks the semantic of the current node.
     *
     * @throws yal.exceptions.AnalyseSemantiqueException The semantic is incorrect.
     */
    public abstract void check();

    /**
     * Returns the MIPS assembly for the current node.
     *
     * @return MIPS assembly for the current node.
     */
    public abstract String toMIPS();
}
