package yal.tree.tos.entry;

import yal.tree.expressions.Expression;

import java.util.ArrayList;
import java.util.Objects;

public final class Function extends Entry {
    private int parameters;
    /**
     * Function constructor.
     * A function can only be of type INTEGER.
     *
     * @param name Name of the function.
     */
    public Function(final String name, int param) {
        super(name, Expression.Type.INTEGER);

        this.parameters = param;
    }


    public int getParameters(){
        return parameters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Entry entry = (Entry) o;

        return Objects.equals(name, entry.name) && type == entry.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getClass(), name, type);
    }
}
