package yal.tree.tos.entry;

import yal.tree.expressions.Expression;

import java.util.Objects;

public final class Variable extends Entry {
    /**
     * Variable constructor.
     * A variable can only be of type INTEGER.
     *
     * @param name Name of the variable.
     */
    public Variable(final String name) {
        super(name, Expression.Type.INTEGER);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Entry entry = (Entry) o;

        return Objects.equals(name, entry.name) && type == entry.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getClass(), name, type);
    }
}
