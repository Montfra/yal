package yal.tree.tos.entry;

import yal.tree.expressions.Expression;

/**
 * Entry of the TOS.
 */
public abstract class Entry {
    /**
     * Name of the entry.
     */
    protected final String name;

    /**
     * Type of the entry.
     */
    protected final Expression.Type type;

    /**
     * Entry constructor.
     *
     * @param name Name of the entry.
     */
    public Entry(final String name, final Expression.Type type) {
        if (name == null) {
            throw new IllegalArgumentException("null name");
        }

        if (type == null) {
            throw new IllegalArgumentException("null type");
        }

        this.name = name;
        this.type = type;
    }

    /**
     * Returns the name of the entry.
     *
     * @return Name of the entry.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the type of the entry.
     *
     * @return Type of the entry.
     */
    public Expression.Type getType() {
        return this.type;
    }
}
