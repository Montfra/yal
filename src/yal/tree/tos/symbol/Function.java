package yal.tree.tos.symbol;

public final class Function extends Symbol {
    private final String label;
    private final int noblock;
    private final int size;

    public Function(final String label, final int noblock, final int size) {
        this.label = label;
        this.noblock = noblock;
        this.size = size;
    }

    public int getNoblock() {
        return noblock;
    }

    public String getLabel() {
        return this.label;
    }

    public int getSize() {
        return size;
    }
}
