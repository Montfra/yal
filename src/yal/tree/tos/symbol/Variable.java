package yal.tree.tos.symbol;

public final class Variable extends Symbol {
    /**
     * Offset of the symbol.
     */
    private final int offset;

    /**
     * Block number of the symbol.
     */
    private final int noblock;

    /**
     * Variable constructor.
     *
     * @param offset Offset of the variable.
     * @param noblock Block number of the variable.
     */
    public Variable(final int offset, int noblock) {
        this.offset = offset;
        this.noblock = noblock;
    }

    /**
     * Returns the offset of the variable.
     *
     * @return Offset of the variable.
     */
    public int getOffset() {
        return this.offset;
    }

    /**
     * Returns the block number of the variable.
     *
     * @return Block number of the variable.
     */
    public int getNoblock() {
        return noblock;
    }
}
