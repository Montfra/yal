package yal.tree.tos;

import yal.exceptions.AnalyseSemantiqueException;
import yal.tree.tos.entry.Function;
import yal.tree.tos.entry.Variable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Table of symbols.
 * Singleton.
 */
public final class TOS {
    /**
     * Singleton instance.
     */
    private static final TOS instance;

    static {
        instance = new TOS();
    }

    /**
     * Returns the singleton instance of the TOS.
     *
     * @return Singleton instance of the TOS.
     */
    public static TOS getInstance() {
        return instance;
    }

    private final ArrayList<HashMap<Variable, yal.tree.tos.symbol.Variable>> variables;
    private final ArrayList<HashMap<Function, yal.tree.tos.symbol.Function>> functions;
    private ArrayList<Function> parameters;
    private final ArrayList<Integer> stack;
    private int stackHead;

    /**
     * TOS constructor.
     */
    private TOS() {
        parameters = new ArrayList<>();

        this.variables = new ArrayList<>(1);
        this.functions = new ArrayList<>(1);

        this.stack = new ArrayList<>(0);
        this.stackHead = -1;
    }

    public int indent() {
        this.variables.add(new HashMap<>());
        this.functions.add(new HashMap<>());

        this.stack.add(this.variables.size() - 1);
        this.stackHead++;

        return this.stack.get(this.stackHead);
    }

    public void outdent() {
        if (this.stackHead == -1) {
            throw new IndexOutOfBoundsException("outdent was called when there was no block");
        } else {
            this.stackHead--;
            this.stack.remove(this.stack.size() - 1);
            this.stack.trimToSize();
        }
    }

    public void push(final int lineNumber, final Variable variable) {
        if (variable == null) { // variable must not be null
            throw new IllegalArgumentException("null variable");
        }

        if (this.stackHead == -1) {
            throw new IndexOutOfBoundsException("not in a block");
        }

        if (this.variables.get(this.stack.get(this.stackHead)).containsKey(variable)) {
            throw new AnalyseSemantiqueException(lineNumber, String.format("symbole deja declare: %s", variable.getName()));
        }

        final int offset = this.variables.get(this.stack.get(this.stackHead)).size() * 4;

        this.variables.get(this.stack.get(this.stackHead)).put(variable, new yal.tree.tos.symbol.Variable(offset, this.stack.get(this.stackHead)));
    }

    public void push(final int lineNumber, final Function function, final String label, final int size) {
        if (function == null) { // function must not be null
            throw new IllegalArgumentException("null function");
        }

        if (this.stackHead == -1) {
            throw new IndexOutOfBoundsException("not in a block");
        }

        if (this.functions.get(this.stack.get(this.stackHead)).containsKey(function)) {
            throw new AnalyseSemantiqueException(lineNumber, String.format("symbole deja declare: %s", function.getName()));
        }

        this.parameters.add(function);
        this.functions.get(this.stack.get(this.stackHead)).put(function, new yal.tree.tos.symbol.Function(label, this.stack.get(this.stackHead), size));
    }

    /**
     * Lookups a symbol from the TOS.
     *
     * @param lineNumber Line number of the current node of the abstract tree.
     * @param entry Entry of the TOS. Must be non-null.
     *
     * @return Variable that corresponds to the entry.
     */
    public yal.tree.tos.symbol.Variable lookup(final int lineNumber, final Variable entry, final int[] block) {
        yal.tree.tos.symbol.Variable symbol = null;

        for (int i = block.length - 1; i >=0; i--) {
            symbol = this.variables.get(block[i]).get(entry);

            if (symbol != null) {
                break;
            }
        }

        if (symbol == null) {
            throw new AnalyseSemantiqueException(lineNumber, String.format("symbole inconnu: %s", entry.getName()));
        }

        return symbol;
    }

    public yal.tree.tos.symbol.Function lookup(final int lineNumber, final Function entry, final int[] block) {
        yal.tree.tos.symbol.Function symbol = null;

        for (int i = block.length - 1; i >=0; i--) {
            symbol = this.functions.get(block[i]).get(entry);

            if (symbol != null) {
                break;
            }
        }

        if (symbol == null) {
            throw new AnalyseSemantiqueException(lineNumber, String.format("symbole inconnu: %s", entry.getName()));
        }

        return symbol;
    }

    public Function getFunction(String name){
        for (Function f : parameters){
            if (f.getName().equals(name)){
                return f;
            }
        }
        return null;
    }

    /**
     * Returns the size of the TOS's symbols in bytes.
     *
     * @return Size of the TOS's symbols in bytes.
     */
    public int size() {
        return this.variables.get(0).size() * 4;
    }

    public int getSizeOf(Integer nob) {
        return this.variables.get(nob).size() * 4;
    }

    public int[] getBlock() {
        return this.stack.stream().mapToInt(value -> value).toArray();
    }
}
