package yal.tree;

import yal.tree.tos.TOS;
import yal.tree.tos.entry.Function;
import yal.util.MIPSBuilder;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Root of the program.
 * Singleton.
 */
public final class Root extends AbstractTree {
    private static final Root instance;

    static {
        instance = new Root();
    }

    public static Root getInstance() {
        return instance;
    }

    private final HashMap<String, InstructionsBlock> functions;

    /**
     * Size allocated to the stack.
     */
    private int stack_size;

    /**
     * Instructions of the current block.
     */
    private InstructionsBlock instructions;

    /**
     * Root constructor.
     */
    private Root() {
        super(1);

        this.functions = new HashMap<>();
    }

    public void setInstructions(final InstructionsBlock instructions) {
        this.instructions = instructions;
    }

    public void addFunction(final int lineNumber, final String name, final InstructionsBlock instructions, ArrayList<String> parameters, final Integer nob) {
        int index = 0;

        while (this.functions.containsKey(name + "_" + index)) {
            index++;
        }

        this.functions.put(name + "_" + index, instructions);

        int param = 0;
        if (parameters != null){
            param = parameters.size();
        }

        TOS.getInstance().push(lineNumber, new yal.tree.tos.entry.Function(name, param), name + "_" + index, TOS.getInstance().getSizeOf(nob));
    }

    @Override
    public void check() {
        this.stack_size = TOS.getInstance().size();

        this.instructions.check();

        this.functions.forEach((s, instructionsBlock) -> instructionsBlock.check());
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        mips.print(this.build_main());
        mips.print(this.instructions.toMIPS());
        mips.print(this.build_end());
        mips.print(this.build_functions());

        return mips.toString();
    }

    /**
     * Builds the main part of the program.
     *
     * @return Main part of the program.
     */
    private String build_main() {
        final MIPSBuilder mips = new MIPSBuilder();

        mips.print(".data\n");
        mips.buildl("message_runtime_error: .asciiz \"ERREUR EXECUTION\"");
        mips.buildl("boolean_false: .asciiz \"faux\"");
        mips.buildl("boolean_true: .asciiz \"vrai\"");

        mips.print(".text\n");

        mips.print("main:\n");
        mips.buildl("move $s7, $sp");

        mips.buildf("addi $sp, $sp, %d", -this.stack_size);

        for (int i = 0; i > -this.stack_size; i -= 4) {
            mips.buildf("sw $zero, %d($s7)", i);
        }

        return mips.toString();
    }

    /**
     * Builds the end part of the program.
     *
     * @return End part of the program.
     */
    private String build_end() {
        final MIPSBuilder mips = new MIPSBuilder();

        mips.print("end:\n");
        mips.buildl("li $v0, 0xA");
        mips.buildl("syscall");

        mips.print("runtime_error:\n");
        mips.buildl("la $a0, message_runtime_error");
        mips.buildl("li $v0, 4");
        mips.buildl("syscall");
        mips.buildl("j end");

        return mips.toString();
    }


    private String build_functions() {
        final MIPSBuilder mips = new MIPSBuilder();

        this.functions.forEach((label, instructionsBlock) -> {
            mips.print(label + ":" + "\n");
            mips.print(instructionsBlock.toMIPS());
        });

        return mips.toString();
    }
}
