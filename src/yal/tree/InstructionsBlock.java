package yal.tree;

import yal.util.MIPSBuilder;

import java.util.ArrayList;

/**
 * Collection of YAL instructions.
 */
public final class InstructionsBlock extends AbstractTree {
    protected final ArrayList<AbstractTree> instructions;

    /**
     * InstructionsBlock constructor.
     *
     * @param lineNumber Line number that caused the creation of the node. Must be positive.
     */
    public InstructionsBlock(final int lineNumber) {
        super(lineNumber);

        this.instructions = new ArrayList<>();
    }

    /**
     * Adds an instruction to the block.
     *
     * @param tree Instruction to add to the block.
     */
    public void add(final AbstractTree tree) {
        this.instructions.add(tree);
    }

    @Override
    public String toString() {
        return this.instructions.toString();
    }

    @Override
    public void check() {
        this.instructions.forEach(AbstractTree::check);
    }

    @Override
    public String toMIPS() {
        final MIPSBuilder mips = new MIPSBuilder();

        this.instructions.forEach(arbreAbstrait -> mips.print(arbreAbstrait.toMIPS()));

        return mips.toString();
    }

}
