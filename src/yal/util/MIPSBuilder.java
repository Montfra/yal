package yal.util;

/**
 * Utility class that encapsulates a StringBuilder for building MIPS instructions.
 */
public final class MIPSBuilder {
    /**
     * Internal StringBuilder.
     */
    private final StringBuilder builder;

    /**
     * MIPSBuilder constructor.
     */
    public MIPSBuilder() {
        this.builder = new StringBuilder();
    }

    /**
     * Print line writes a MIPS instruction with a single level of indentation and a line break.
     *
     * @param instruction MIPS instruction to build. Must be non null.
     */
    public void buildl(final String instruction) {
        if (instruction == null) {
            throw new IllegalArgumentException("null instruction");
        }

        this.builder.append('\t').append(instruction).append('\n');
    }

    /**
     * Print format is a shorthand for this.buildl(String.format(format, args)).
     *
     * @param format Format for String.format.
     * @param args Parameters for String.format.
     */
    public void buildf(final String format, final Object... args) {
        if (format == null) {
            throw new IllegalArgumentException("null format");
        }

        if (args == null) {
            throw new IllegalArgumentException("null args");
        }

        this.builder.append(String.format(String.format("\t%s\n", format), args));
    }

    /**
     * Print writes a raw String to the output without any processing.
     *
     * @param raw Raw string.
     */
    public void print(final String raw) {
        if (raw == null) {
            throw new IllegalArgumentException("null raw");
        }

        this.builder.append(raw);
    }

    /**
     * Label creates a label with format prefix_name_suffix
     *
     * @param prefix Prefix of the label. Must not be null.
     * @param name Name of the label. Must not be null.
     * @param suffix Suffix of the label. Must be positive.
     */
    public void label(final String prefix, final String name, final int suffix) {
        if (prefix == null) { // prefix must not be null
            throw new IllegalArgumentException("null prefix");
        }

        if (name == null) { // name must not be null
            throw new IllegalArgumentException("null name");
        }

        this.builder.append(String.format("%s_%s_%d:\n", prefix, name, suffix));
    }

    @Override
    public String toString() {
        return this.builder.toString();
    }
}
