package yal;

import yal.analyse.Lexer;
import yal.analyse.Parser;
import yal.exceptions.AnalyseException;
import yal.tree.AbstractTree;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Yal {

    public Yal(String nomFichier) {
        try {
            Parser analyseur = new Parser(new Lexer(new FileReader(nomFichier)));
            AbstractTree arbre = (AbstractTree) analyseur.parse().value;

            arbre.check();
            System.out.println("COMPILATION OK");

            String nomSortie = nomFichier.replaceAll("[.]yal", ".mips");
            PrintWriter flot = new PrintWriter(new BufferedWriter(new FileWriter(nomSortie)));
            flot.println(arbre.toMIPS());
            flot.close();
        } catch (FileNotFoundException ex) {
            System.err.println("Fichier " + nomFichier + " inexistant");
        } catch (AnalyseException ex) {
            System.err.println(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(yal.Yal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Nombre incorrect d'arguments");
            System.err.println("\tjava -jar yal.jar <fichierSource.yal>");
            System.exit(1);
        }
        new yal.Yal(args[0]);
    }

}
