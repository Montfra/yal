package yal.exceptions;

public class FunctionArgumentException extends RuntimeException {
    public FunctionArgumentException(int ligne){super("ERREUR ARGUMENTS FONCTION ligne " + ligne);}
}
