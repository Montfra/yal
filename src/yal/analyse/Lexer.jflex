package yal.analyse ;

import java_cup.runtime.*;
import yal.exceptions.AnalyseLexicaleException;
      
%%
   
%class Lexer
%public

%line
%column

%type Symbol
%eofval{
        return symbol(Tokens.EOF) ;
%eofval}

%cup

%{

  private StringBuilder chaine ;

  private Symbol symbol(int type) {
	return new Symbol(type, yyline, yycolumn) ;
  }

  private Symbol symbol(int type, Object value) {
	return new Symbol(type, yyline, yycolumn, value) ;
  }
%}

%xstate Chaine

idf = [A-Za-z_][A-Za-z_0-9]*

csteE = [0-9]+

finDeLigne = \r|\n
espace = {finDeLigne}  | [ \t\f]

line_comment = \/\/[^\n]*


%%

{line_comment}         {}

"programme"            { return symbol(Tokens.PROGRAM); }
"debut"                { return symbol(Tokens.DEBUT); }
"fin"              	   { return symbol(Tokens.FIN); }

"fonction"             { return symbol(Tokens.FONCTION); }
"retourne"             { return symbol(Tokens.RETOURNE); }

// IF statement
"si"                   { return symbol(Tokens.SI); }
"alors"                { return symbol(Tokens.ALORS); }
"sinon"                { return symbol(Tokens.SINON); }
"finsi"                { return symbol(Tokens.FINSI); }

// WHILE statement
"tantque"              { return symbol(Tokens.TANTQUE); }
"repeter"              { return symbol(Tokens.REPETER); }
"fintantque"           { return symbol(Tokens.FINTANTQUE); }

// Expression
"("                    { return symbol(Tokens.PAR_OPEN); }
")"                    { return symbol(Tokens.PAR_CLOSE); }
"["                    { return symbol(Tokens.BRPEN);}
"]"                    { return symbol(Tokens.BRCLOSE);}

// Operation
"+"                    { return symbol(Tokens.PLUS); }
"-"                    { return symbol(Tokens.HYPHEN); }
"*"                    { return symbol(Tokens.STAR); }
"/"                    { return symbol(Tokens.SLASH); }
"<"                    { return symbol(Tokens.LEFT_ANGLE_BRACKET); }
">"                    { return symbol(Tokens.RIGHT_ANGLE_BRACKET); }
"=="                   { return symbol(Tokens.EQUAL_EQUAL); }
"!="                   { return symbol(Tokens.NOT_EQUAL); }
"et"                   { return symbol(Tokens.AND); }
"ou"                   { return symbol(Tokens.OR); }
"non"                  { return symbol(Tokens.NOT); }

"ecrire"               { return symbol(Tokens.ECRIRE); }
"lire"               { return symbol(Tokens.LIRE); }

"entier"               { return symbol(Tokens.ENTIER); }

";"                    { return symbol(Tokens.EOL); }
","                    { return symbol(Tokens.COMMA); }

"="                    { return symbol(Tokens.ASSIGN); }

{csteE}      	       { return symbol(Tokens.CONST_INTEGER, yytext()); }

{idf}      	           { return symbol(Tokens.IDF, yytext()); }

{espace}               { }
.                      { throw new AnalyseLexicaleException(yyline, yycolumn, yytext()) ; }

