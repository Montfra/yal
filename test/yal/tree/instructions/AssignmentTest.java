package yal.tree.instructions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import yal.tree.expressions.Expression;
import yal.tree.tos.TOS;
import yal.tree.tos.entry.Entry;
import yal.tree.tos.symbol.Symbol;

final class AssignmentTest {
    private static final String DUMMY_VARIABLE_NAME_1;
    private static final String DUMMY_VARIABLE_NAME_2;

    static {
        DUMMY_VARIABLE_NAME_1 = "DUMMY_VARIABLE_NAME_1_" + AssignmentTest.class.hashCode();
        DUMMY_VARIABLE_NAME_2 = "DUMMY_VARIABLE_NAME_2_" + AssignmentTest.class.hashCode();
    }

    private static final class DummyExpressionThrow extends Expression {
        static final String DUMMY_VALUE = "dummy_value_" + DummyExpressionThrow.class.hashCode();

        static final class DummyException extends RuntimeException {
        }

        private DummyExpressionThrow() {
            super(0, Type.INTEGER);
        }

        @Override
        public void check() {
            throw new DummyException();
        }

        @Override
        public String toMIPS() {
            return DUMMY_VALUE;
        }
    }

    private static final class DummyExpression extends Expression {
        static final String DUMMY_VALUE = "dummy_value_" + DummyExpression.class.hashCode();

        private DummyExpression() {
            super(0, Type.INTEGER);
        }

        @Override
        public void check() {
            // do nothing
        }

        @Override
        public String toMIPS() {
            return DUMMY_VALUE;
        }
    }

    private Assignment assignment1;
    private Assignment assignment2;

    @BeforeAll
    static void initialize() {
        TOS.getInstance().push(0, new Entry(DUMMY_VARIABLE_NAME_1, Expression.Type.INTEGER));
        TOS.getInstance().push(0, new Entry(DUMMY_VARIABLE_NAME_2, Expression.Type.INTEGER));
    }

    @BeforeEach
    void setUp() {
        assignment1 = new Assignment(0, DUMMY_VARIABLE_NAME_1, new DummyExpression());
        assignment2 = new Assignment(0, DUMMY_VARIABLE_NAME_2, new DummyExpressionThrow());
    }

    @Test
    void verifier() {
//        Assertions.assertDoesNotThrow(assignment1::check);
        Assertions.assertThrows(DummyExpressionThrow.DummyException.class, assignment2::check);
    }

    @Test
    void toMIPS() {
        assignment1.check();
        Assertions.assertEquals(String.format("%s\tsw $t0, %d($s7)\n", DummyExpression.DUMMY_VALUE, TOS.getInstance().lookup(0, new Entry(DUMMY_VARIABLE_NAME_1, Expression.Type.INTEGER)).getOffset()), assignment1.toMIPS());
    }
}
