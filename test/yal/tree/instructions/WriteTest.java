package yal.tree.instructions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import yal.tree.expressions.Expression;
import yal.tree.instructions.io.Write;

final class WriteTest {
    private static final class DummyExpressionThrow extends Expression {
        static final String DUMMY_VALUE = "dummy_value_" + DummyExpressionThrow.class.hashCode();

        static final class DummyException extends RuntimeException {
        }

        private DummyExpressionThrow() {
            super(0, Type.INTEGER);
        }

        @Override
        public void check() {
            throw new DummyException();
        }

        @Override
        public String toMIPS() {
            return DUMMY_VALUE;
        }
    }

    private static final class DummyExpression extends Expression {
        static final String DUMMY_VALUE = "dummy_value_" + DummyExpression.class.hashCode();

        private DummyExpression() {
            super(0, Type.INTEGER);
        }

        @Override
        public void check() {
            // do nothing
        }

        @Override
        public String toMIPS() {
            return DUMMY_VALUE;
        }
    }

    private Write write1;
    private Write write2;

    @BeforeEach
    void setUp() {
        write1 = new Write(0, new DummyExpressionThrow());
        write2 = new Write(0, new DummyExpression());
    }

    @Test
    void verifier() {
        Assertions.assertThrows(DummyExpressionThrow.DummyException.class, write1::check);

//        Assertions.assertDoesNotThrow(write2::check);
    }

    @Test
    void toMIPS() {
        Assertions.assertEquals(DummyExpression.DUMMY_VALUE + "\tmove $a0, $t0\n" + "\tli $v0, 0x1\n" + "\tsyscall\n" + "\tli $a0, 0xA\n" + "\tli $v0, 0xB\n" + "\tsyscall\n", write2.toMIPS());
    }
}
