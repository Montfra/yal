package yal.tree.instructions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import yal.tree.expressions.Expression;
import yal.tree.instructions.io.Read;
import yal.tree.tos.TOS;
import yal.tree.tos.entry.Entry;
import yal.tree.tos.symbol.Symbol;

class ReadTest {
    private static final String DUMMY_VARIABLE_NAME;

    static {
        DUMMY_VARIABLE_NAME = "DUMMY_VARIABLE_NAME_" + ReadTest.class.hashCode();
    }

    private Read lire;

    @BeforeAll
    static void initialize() {
        TOS.getInstance().push(0, new Entry(DUMMY_VARIABLE_NAME, Expression.Type.INTEGER));
    }

    @BeforeEach
    void setUp() {
        lire = new Read(0, DUMMY_VARIABLE_NAME);
    }

    @Test
    void verifier() {
//        Assertions.assertDoesNotThrow(lire::check);
    }

    @Test
    void toMIPS() {
        lire.check();
        Assertions.assertEquals("\tli $v0, 0x5\n" + "\tsyscall\n" + String.format("\tsw $v0, %d($s7)\n", TOS.getInstance().lookup(0, new Entry(DUMMY_VARIABLE_NAME, Expression.Type.INTEGER)).getOffset()), lire.toMIPS());
    }
}
