package yal.tree;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class BlocDInstructionsTest {
    private static final class DummyArbreAbstraitThrow extends AbstractTree {
        static final String DUMMY_VALUE = "dummy_value_" + DummyArbreAbstraitThrow.class.hashCode();

        static final class DummyException extends RuntimeException {
        }

        private DummyArbreAbstraitThrow() {
            super(0);
        }

        @Override
        public void check() {
            throw new DummyException();
        }

        @Override
        public String toMIPS() {
            return DUMMY_VALUE;
        }
    }

    private static final class DummyArbreAbstrait extends AbstractTree {
        static final String DUMMY_VALUE = "dummy_value_" + DummyArbreAbstrait.class.hashCode();

        static final class DummyException extends RuntimeException {
        }

        private DummyArbreAbstrait() {
            super(0);
        }

        @Override
        public void check() {
            // do nothing
        }

        @Override
        public String toMIPS() {
            return DUMMY_VALUE;
        }
    }

    private InstructionsBlock blocDInstructions1;
    private InstructionsBlock blocDInstructions2;
    private InstructionsBlock blocDInstructions3;
    private InstructionsBlock blocDInstructions4;

    @BeforeEach
    void setUp() {
        blocDInstructions1 = new InstructionsBlock(0);
        blocDInstructions1.add(new DummyArbreAbstraitThrow());

        blocDInstructions2 = new InstructionsBlock(0);
        blocDInstructions2.add(new DummyArbreAbstraitThrow());
        blocDInstructions2.add(new DummyArbreAbstraitThrow());

        blocDInstructions3 = new InstructionsBlock(0);
        blocDInstructions3.add(new DummyArbreAbstrait());

        blocDInstructions4 = new InstructionsBlock(0);
        blocDInstructions4.add(new DummyArbreAbstrait());
        blocDInstructions4.add(new DummyArbreAbstrait());
    }

    @Test
    void verifier() {
        Assertions.assertThrows(DummyArbreAbstraitThrow.DummyException.class, blocDInstructions1::check);
        Assertions.assertThrows(DummyArbreAbstraitThrow.DummyException.class, blocDInstructions2::check);
//        Assertions.assertDoesNotThrow(blocDInstructions3::check);
//        Assertions.assertDoesNotThrow(blocDInstructions4::check);
    }

    @Test
    void toMIPS() {
        blocDInstructions3.check();
        Assertions.assertEquals(DummyArbreAbstrait.DUMMY_VALUE, blocDInstructions3.toMIPS());

        blocDInstructions4.check();
        Assertions.assertEquals(DummyArbreAbstrait.DUMMY_VALUE + DummyArbreAbstrait.DUMMY_VALUE, blocDInstructions4.toMIPS());
    }
}
