package yal.tree.expressions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import yal.tree.expressions.Expression.Type;
import yal.tree.tos.TOS;
import yal.tree.tos.entry.Entry;
import yal.tree.tos.symbol.Symbol;

final class VariableTest {
    private static final String DUMMY_VARIABLE_NAME_1;
    private static final String DUMMY_VARIABLE_NAME_2;

    static {
        DUMMY_VARIABLE_NAME_1 = "DUMMY_VARIABLE_NAME_1_" + VariableTest.class.hashCode();
        DUMMY_VARIABLE_NAME_2 = "DUMMY_VARIABLE_NAME_2_" + VariableTest.class.hashCode();
    }

    private Variable variable1;
    private Variable variable2;

    @BeforeAll
    static void initialize() {
        TOS.getInstance().push(0, new Entry(DUMMY_VARIABLE_NAME_1, Type.INTEGER));
        TOS.getInstance().push(0, new Entry(DUMMY_VARIABLE_NAME_2, Type.INTEGER));
    }

    @BeforeEach
    void setUp() {
        variable1 = new Variable(0, DUMMY_VARIABLE_NAME_1);
        variable2 = new Variable(0, DUMMY_VARIABLE_NAME_2);
    }

    @Test
    void verifier() {
//        Assertions.assertDoesNotThrow(variable1::check);
//        Assertions.assertDoesNotThrow(variable2::check);
    }

    @Test
    void toMIPS() {
        variable1.check();
        Assertions.assertEquals(String.format("\tlw $t0, %d($s7)\n", TOS.getInstance().lookup(0, new Entry(DUMMY_VARIABLE_NAME_1, Type.INTEGER)).getOffset()), variable1.toMIPS());

        variable2.check();
        Assertions.assertEquals(String.format("\tlw $t0, %d($s7)\n", TOS.getInstance().lookup(0, new Entry(DUMMY_VARIABLE_NAME_2, Type.INTEGER)).getOffset()), variable2.toMIPS());
    }
}
