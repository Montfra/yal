package yal.tree.expressions.literals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class IntegerLiteralTest {
    private static final String DUMMY_INTEGER_LITERAL_POSITIVE;
    private static final String DUMMY_INTEGER_LITERAL_NEGATIVE;

    static {
        DUMMY_INTEGER_LITERAL_POSITIVE = Integer.toString(Math.abs("DUMMY_INTEGER_LITERAL_POSITIVE".hashCode()));
        DUMMY_INTEGER_LITERAL_NEGATIVE = Integer.toString(-Math.abs("DUMMY_INTEGER_LITERAL_NEGATIVE".hashCode()));
    }

    private IntegerLiteral integerLiteralPositive;
    private IntegerLiteral integerLiteralNegative;

    @BeforeEach
    void setUp() {
        integerLiteralPositive = new IntegerLiteral(0, DUMMY_INTEGER_LITERAL_POSITIVE);
        integerLiteralNegative = new IntegerLiteral(0, DUMMY_INTEGER_LITERAL_NEGATIVE);
    }

    @Test
    void toMIPS() {
        Assertions.assertEquals(String.format("\tli $t0, %s\n", DUMMY_INTEGER_LITERAL_POSITIVE), integerLiteralPositive.toMIPS());
        Assertions.assertEquals(String.format("\tli $t0, %s\n", DUMMY_INTEGER_LITERAL_NEGATIVE), integerLiteralNegative.toMIPS());
    }
}
